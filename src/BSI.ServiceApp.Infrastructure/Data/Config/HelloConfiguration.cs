using BSI.ServiceApp.Core.ProjectAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BSI.ServiceApp.Infrastructure.Data.Config;

public class HelloConfiguration : IEntityTypeConfiguration<Hello>
{
  public void Configure(EntityTypeBuilder<Hello> builder)
  {
    builder.Property(t => t.Title)
        .IsRequired();
  }
}
