﻿using Autofac;
using BSI.ServiceApp.Core.Interfaces;
using BSI.ServiceApp.Core.Services;

namespace BSI.ServiceApp.Core;

public class DefaultCoreModule : Module
{
  protected override void Load(ContainerBuilder builder)
  {
    builder.RegisterType<ToDoItemSearchService>()
        .As<IToDoItemSearchService>().InstancePerLifetimeScope();
  }
}
