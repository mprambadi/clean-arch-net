﻿namespace BSI.ServiceApp.Core.ProjectAggregate;

public enum ProjectStatus
{
  InProgress,
  Complete
}
