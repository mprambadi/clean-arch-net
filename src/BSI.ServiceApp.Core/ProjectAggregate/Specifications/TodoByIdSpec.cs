﻿using Ardalis.Specification;

namespace BSI.ServiceApp.Core.ProjectAggregate.Specifications;

public class TodoByIdSpec : Specification<ToDoItem>
{
  public TodoByIdSpec(int todoId)
  {
    Query
        .Where(item => item.Id==todoId);
  }
}
