﻿using Ardalis.Specification;
using BSI.ServiceApp.Core.ProjectAggregate;

namespace BSI.ServiceApp.Core.ProjectAggregate.Specifications;

public class ProjectByIdWithItemsSpec : Specification<Project>, ISingleResultSpecification
{
  public ProjectByIdWithItemsSpec(int projectId)
  {
    Query
        .Where(project => project.Id == projectId)
        .Include(project => project.Items);
  }
}
