using BSI.ServiceApp.Core.ProjectAggregate.Events;
using BSI.ServiceApp.SharedKernel;
using BSI.ServiceApp.SharedKernel.Interfaces;


namespace BSI.ServiceApp.Core.ProjectAggregate;

public class Hello : EntityBase, IAggregateRoot
{
  public string Title { get; set; } = string.Empty;
  public string Description { get; set; } = string.Empty;
  public bool IsDone { get; private set; }


  public override string ToString()
  {
    string status = IsDone ? "Done!" : "Not done.";
    return $"{Id}: Status: {status} - {Title} - {Description}";
  }
}
