﻿namespace BSI.ServiceApp.Web.Endpoints.ProjectEndpoints;

public record ProjectRecord(int Id, string Name);
