﻿
namespace BSI.ServiceApp.Web.Endpoints.TodoEndpoints;

public class GetTodoByIdResponse
{
  public GetTodoByIdResponse(int id, string title, string description, bool isDone)
  {
    Id = id;
    Title = title;
    Description = description;
    IsDone = isDone;
  }

  public int Id { get; set; }
  public string Title { get; set; }
  public string Description { get; set; }
  public bool IsDone { get; set; }

}
