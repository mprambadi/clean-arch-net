﻿
namespace BSI.ServiceApp.Web.Endpoints.TodoEndpoints;

public class GetTodoByIdRequest
{
  public const string Route = "/Todo/{TodoId:int}";
  public static string BuildRoute(int todoId) => Route.Replace("{TodoId:int}", todoId.ToString());

  public int TodoId { get; set; }
}
