﻿using Ardalis.ApiEndpoints;
using BSI.ServiceApp.Core.ProjectAggregate;
using BSI.ServiceApp.Core.ProjectAggregate.Specifications;
using BSI.ServiceApp.SharedKernel.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace BSI.ServiceApp.Web.Endpoints.TodoEndpoints;

public class GetById : EndpointBaseAsync
  .WithRequest<GetTodoByIdRequest>
  .WithActionResult<GetTodoByIdResponse>
{
  private readonly IRepository<ToDoItem> _repository;

  public GetById(IRepository<ToDoItem> repository)
  {
    _repository = repository;
  }

  [HttpGet(GetTodoByIdRequest.Route)]
  [SwaggerOperation(
    Summary = "Gets a single Todo",
    Description = "Gets a single Todo by Id",
    OperationId = "Todo.GetById",
    Tags = new[] { "TodoEndpoints" })
  ]
  public override async Task<ActionResult<GetTodoByIdResponse>> HandleAsync(
    [FromRoute] GetTodoByIdRequest request,
    CancellationToken cancellationToken = new())
  {
    var spec = new TodoByIdSpec(request.TodoId);
    var entity = await _repository.FirstOrDefaultAsync(spec, cancellationToken);
    if (entity == null)
    {
      return NotFound();
    }

    var response = new GetTodoByIdResponse
    (
      id: entity.Id,
      title: entity.Title,
      description: entity.Description,
      isDone: entity.IsDone
    );

    return Ok(response);
  }
}
