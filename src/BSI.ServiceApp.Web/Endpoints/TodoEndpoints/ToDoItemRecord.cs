﻿namespace BSI.ServiceApp.Web.Endpoints.TodoEndpoints;

public record ToDoItemRecord(int Id, string Title, string Description, bool IsDone);
